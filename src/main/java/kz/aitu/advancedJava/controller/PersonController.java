package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.PersonRepository;
import kz.aitu.advancedJava.service.PersonService;
import kz.aitu.advancedJava.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@Controller
public class PersonController {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PersonService personService;

    public PersonController(PersonService personService) {

        this.personService = personService;
    }



    @GetMapping("/api/Person")
    public ResponseEntity<?> getPerson() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(personRepository.findAll());
    }


}
